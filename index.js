const express = require("express");
app.use(express.static("public"));
const app = express();


app.get("/", function(req, res){
  res.sendFile(__dirname + "/index.html");
});

app.listen(8080, function(){
  console.log("server started at port 8080");
});